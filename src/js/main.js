$('#parallax').parallax({
    invertX: true,
    invertY: true,
    scalarX: 10,
    frictionY: .1
});

// mobile menu -->
$('.menu-burger').click(function () {
    $(this).toggleClass('active');
    $('.mobile-menu-wrap').toggle();
});
// mobile menu end <--
